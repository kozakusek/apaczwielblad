package com.example.cameloridkimnotegyptian;

import org.apache.camel.Exchange;

import java.io.File;
import java.util.Objects;
import java.util.Scanner;

public class MailTransformator implements org.apache.camel.Processor {
    @Override
    public void process(Exchange exchange) throws Exception {
        File dir = new File("C:/CamelCurrencies");
        String body = "To za oknem chyba tak:\n";

        Scanner pogodynka = new Scanner(new File("C:/CamelPogoda/pogoda.txt"));
        while (pogodynka.hasNextLine()) {
            body += pogodynka.nextLine() + "\n";
        }

        body += "\nA gdby kto chcial bawic sie w krypto:\n";
        for (File file : Objects.requireNonNull(dir.listFiles())) {
            if (file.isFile()) {
                Scanner fs = new Scanner(file);
                String last = "0";
                String prev = "";
                while (fs.hasNextLine()) {
                    prev = last;
                    last = fs.nextLine();
                }
                body += last;
                Double prevVal = Double.parseDouble(prev.split(" ")[1]);
                Double diff = Double.parseDouble(last.split(" ")[1]) - prevVal;
                body += " (" + (diff.doubleValue() >= 0.d ? "+" : "");
                body += String.valueOf(diff / prevVal) + "%)\n";
            }
        }

        body += "Do zobaczenia za 4h :)\n";

        exchange.getIn().setHeader("subject", "Gdybyście nie mieli okna");
        exchange.getIn().setBody(body);
    }
}
