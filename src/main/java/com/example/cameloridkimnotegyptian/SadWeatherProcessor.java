package com.example.cameloridkimnotegyptian;

import org.apache.camel.Exchange;

public class SadWeatherProcessor implements org.apache.camel.Processor {
    @Override
    public void process(Exchange exchange) throws Exception {
        String in = exchange.getIn().getBody(String.class);
        String body = "";

        body += in.substring(in.indexOf("description") + 13, in.indexOf(",\"icon\":")) + "\n";
        body += "Temperaturka - niby ";
        body += in.substring(in.indexOf("temp\"") + 6, in.indexOf(",\"feels_like"));
        body += ", ale czuc " + in.substring(in.indexOf("feels_like") + 12, in.indexOf(",\"temp_min")) + "\n";
        body += "Wiaterek natomiast " + in.substring(in.indexOf("speed") + 7, in.indexOf("speed") + 11) + "\n";

        exchange.getIn().setBody(body);
    }
}
