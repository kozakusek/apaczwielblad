package com.example.cameloridkimnotegyptian;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CamelOrIdkImnotEgyptianApplication {

    public static void main(String[] args) {
        SpringApplication.run(CamelOrIdkImnotEgyptianApplication.class, args);
    }

}
