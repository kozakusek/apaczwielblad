package com.example.cameloridkimnotegyptian;

import org.apache.camel.Exchange;

public class AbusedCurrencyWorker implements org.apache.camel.Processor {
    final String cNameId = "instrument=";
    final String cDelim = "/";
    final String cValueId = "avg=";
    final String cValueDelim = ", volume";

    @Override
    public void process(Exchange exchange) throws Exception {
        String in = exchange.getIn().getBody(String.class);

        String currencyName = in.substring(in.indexOf(cNameId)+cNameId.length(), in.indexOf(cDelim));
        String currencyValue = in.substring(in.indexOf(cValueId)+cValueId.length(), in.indexOf(cValueDelim));
        String body = currencyName + ": " + currencyValue + "\n";

        exchange.getIn().setBody(body);
    }
}
