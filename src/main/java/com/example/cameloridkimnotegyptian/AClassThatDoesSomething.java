package com.example.cameloridkimnotegyptian;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;

@Component
public class AClassThatDoesSomething extends RouteBuilder {
    final String[] cryptos = {"BTC", "ETH", "LTC", "ADA"};
    final int mailDelay = 20000;
    @Override
    public void configure() throws Exception {

        from("timer://myTimer?fixedRate=true&period={{api.sendingPeriod}}")
                .to("direct:BTC")
                .to("direct:ETH")
                .to("direct:LTC")
                .to("direct:ADA")
                .to("direct:Rainy")
                .delay(mailDelay).to("direct:mailBecauseNothingWorks");

        for (String cryptoStr : cryptos) {
            from("direct:"+cryptoStr).setBody(simple(cryptoStr + "/USDT"))
                    .to("xchange:binance?service=marketdata&method=ticker")
                    .process(new AbusedCurrencyWorker())
                    .log("${body}")
                    .to("file:C:/CamelCurrencies?fileName="+cryptoStr.toLowerCase()+".txt&fileExist=Append");
        }

        from("direct:Rainy")
                .to("weather:getWeather?location={{api.city}}&appid={{api.weather}}" +
                  "&geolocationAccessKey=IPSTACK_ACCESS_KEY" +
                  "&geolocationRequestHostIP=LOCAL_IP&units=metric&language=pl")
                .process(new SadWeatherProcessor())
                .log("${body}")
                .to("file:C:/CamelPogoda?fileName=pogoda.txt");

        from("direct:mailBecauseNothingWorks")
                .process(new MailTransformator())
                .setHeader("to", simple("{{api.recieverMail}}"))
                .log("${body}")
                .to("smtps://smtp.gmail.com:465?username={{api.senderMail}}&password={{api.senderPassword}}");
    }
}
